﻿using UnityEngine;
using System.Collections;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Model
{	
	[XmlRoot ("Brands")]
	public class BrandData
	{
		public BrandData ()
		{
			brands = new List<BrandDetail> ();
		}

		[XmlElement ("Brand")]
		public List<BrandDetail> brands;
	}

	public class BrandDetail
	{
		[XmlElement (ElementName = "code")]
		public string code;

		[XmlElement (ElementName = "name")]
		public string name;

		[XmlElement (ElementName = "description")]
		public string description;

		[XmlElement (ElementName = "branch")]
		public string branch;

		[XmlElement (ElementName = "allow")]
		public string allow;

		[XmlElement (ElementName = "category")]
		public string category;

		[XmlElement (ElementName = "logo")]
		public string logo;

		[XmlElement (ElementName = "qr")]
		public string qr;

		[XmlElement (ElementName = "typeofBarCode")]
		public int typeofBarCode;

	}
}