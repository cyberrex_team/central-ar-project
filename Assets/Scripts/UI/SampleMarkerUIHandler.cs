﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampleMarkerUIHandler : MonoBehaviour
{
	public Texture2D marker;

	// Call in UI
	void Start ()
	{
		ScreenshotManager.OnImageSaved += ImageSaved;
	}

	void OnEnable()
	{
		AndroidBackButtonController.Instance.Push(()=>
		{
			Hide();
		});
	}

	void OnDestroy ()
	{				
		ScreenshotManager.OnImageSaved -= ImageSaved;
	}

	public void Hide ()
	{
		Destroy (this.gameObject);
	}

	public void OnSaveMarkerClick ()
	{
		ScreenshotManager.SaveImage (marker, "Central-Marker", "png");
	}

	void ImageSaved (string path)
	{
//		console.text += "\n" + marker.name + " finished saving to " + path;
		Instantiate(Resources.Load ("Prefabs/UI/SaveDoneCanvas"));
		Hide ();
	}


}
