﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CategoryCanvasHandler : MonoBehaviour
{
    public Transform _container;
    public Sprite[] brandBG;

    void Start()
    {
        LoadCategoryList();
    }

    void LoadCategoryList()
    {
		int index = 0;
        foreach (string category in CommonConfig.brandCategory.Keys)
        {
			index++;
            GameObject item = Instantiate(Resources.Load("Prefabs/UI/CategoryListItem")) as GameObject;
            item.transform.SetParent(_container);
            item.transform.localScale = Vector3.one;
            CategoryItemHandler _i = item.GetComponent<CategoryItemHandler>();

            _i.categoryName = category;
            _i.bg.sprite = brandBG[index-1];
        }
    }
}
