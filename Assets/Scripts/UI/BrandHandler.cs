﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Model;
using UnityEngine.SceneManagement;

public class BrandHandler : MonoBehaviour
{
	public Transform _content;
	public Text counter;

	// Use this for initialization
	void Start ()
	{
		OnReloaded ();
		DialogCanvasHandler.BrandReload += OnReloaded;
	}

	void OnEnable()
	{
		AndroidBackButtonController.Instance.Push(()=>
		{
			SceneManager.LoadScene("AR");
		});
	}

	void OnDestroy () {
		DialogCanvasHandler.BrandReload -= OnReloaded;
	}
		
	void ClearBrandList () {
		BrandItemHandler[] brands = _content.GetComponentsInChildren<BrandItemHandler> ();

		foreach (BrandItemHandler l in brands) {
			Destroy (l.gameObject);
		}
	}

	void OnReloaded ()
	{
		ClearBrandList ();

		List<BrandDetail> brands = AppManager.Instance.GetMyBrand ();
		int i = 0;

		foreach (BrandDetail brand in brands) 
		{
			i++;
			GameObject b = Instantiate (Resources.Load ("Prefabs/Profile/BrandListItem")) as GameObject;
			b.transform.SetParent (_content);
			b.transform.localScale = Vector3.one;
			
			BrandItemHandler item = b.GetComponent<BrandItemHandler> ();
			item.brandDetail = brand;
			item.brandCode = brand.code;
			item.counter.text = i.ToString();
		}
		counter.text = string.Format("You have {0} items" , i);
	}
}
