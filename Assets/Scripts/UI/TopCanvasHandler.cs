﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TopCanvasHandler : MonoBehaviour
{

	public GameObject backButton;
	public GameObject cartButton;

	void Awake () {		
		if (SceneManager.GetActiveScene ().name == "Cart") {
			cartButton.SetActive (false);
		}
	}

	public void OnBackButtonClick ()
	{
		if (PlayerPrefs.GetInt (CommonConfig.USER_SCAN_DETECTED) == 0 || SceneManager.GetActiveScene().name == "AR") {
			SceneManager.LoadScene ("Main");
		} else {
			SceneManager.LoadScene ("AR");
		}
	}

	public void OnCartButtonClick () {

		if (SceneManager.GetActiveScene ().name == "AR") {
			PlayerPrefs.SetInt (CommonConfig.USER_SCAN_DETECTED, 1);
		} else {
			PlayerPrefs.SetInt (CommonConfig.USER_SCAN_DETECTED, 1);
		}

		SceneManager.LoadScene ("Cart");
	}
}
