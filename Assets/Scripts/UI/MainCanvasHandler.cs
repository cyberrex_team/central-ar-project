﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using UnityEngine.UI;

public class MainCanvasHandler : MonoBehaviour {

	void Start () {
//		if (!SystemInfo.supportsGyroscope) {			
//			Instantiate (Resources.Load ("Prefabs/UI/GyroSupportCanvas"));
//		}

		//Removed marker detected
		PlayerPrefs.SetInt (CommonConfig.USER_SCAN_DETECTED, 0);
	}

	// Call in UI
	public void OnCartClick () {
		SceneManager.LoadScene ("Cart");
	}

	public void OnARClick () {
		SceneManager.LoadScene ("AR");
	}

	public void OnTutorialClick () {
		Instantiate(Resources.Load("Prefabs/UI/TutorialCanvas"));
	}

	public void OnSaveMarkerClick () {
		Instantiate(Resources.Load("Prefabs/UI/SamepleMarkerCanvas"));
	}
}
