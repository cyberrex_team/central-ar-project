﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Model;
using UnityEngine.UI;

public class DialogCanvasHandler : MonoBehaviour
{

	public string brandCode;
	BrandDetail brandDetail;

	public Image logoImage;
	public Image qrImage;

	public Text logoText;
	public Text promotionDescription;
	public Text allowDescription;
	public Text branchDescription;

	public delegate void Callaback ();
	public static event Callaback BrandReload;
	public static event Callaback BrandSaved;

	public GameObject _uiBrand;

	void Start ()
	{		
		brandDetail = CommonConfig.brandData [brandCode];
		UpdateBrandDetail ();
		UpdateQR ();
		UpdateLogo ();
	}

	void OnEnable()
	{
		AndroidBackButtonController.Instance.Push(()=>
		{
			OnCloseButtonClick();
		});
	}

	void UpdateBrandDetail ()
	{
		logoText.text = brandDetail.name;
		promotionDescription.text = brandDetail.description.Replace("NEWLINE", "\n");
		allowDescription.text = brandDetail.allow;
		branchDescription.text = brandDetail.branch;
	}

	void UpdateLogo ()
	{
		logoImage.sprite = Resources.Load<Sprite> ("Sprites/" + brandDetail.logo);
	}

	void UpdateQR ()
	{
		if (qrImage != null) {
			qrImage.sprite = Resources.Load<Sprite> ("Sprites/QRcode/" + brandDetail.typeofBarCode);
		}
	}

	public void OnCancel ()
	{
		Destroy (this.gameObject);
	}

	public void OnRemove ()
	{
		Debug.Log ("Remove coupon " + brandCode);
		AppManager.Instance.RemoveBrand (brandCode);

		BrandReload.Invoke ();
		Destroy (this.gameObject);
	}

	public void OnSave ()
	{
		Debug.Log ("Save coupon " + brandCode);
		AppManager.Instance.AddBrand (brandCode);

		OnCloseButtonClick ();
	}

	public void OnCloseButtonClick ()
	{
		Destroy (_uiBrand);
		Destroy (this.gameObject);
	}
}
