﻿using UnityEngine;

public class TutorialCanvasUIHandler : MonoBehaviour {
	
	void OnEnable()
	{
		AndroidBackButtonController.Instance.Push(()=>
		{
			Destroy(this.gameObject);
		});
	}

	public void Hide ()
	{
		Destroy (this.gameObject);
	}

}
