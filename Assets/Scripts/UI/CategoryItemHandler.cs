﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CategoryItemHandler : MonoBehaviour {

	public delegate void Callaback ();
	public static event Callaback ArReload;

	public string categoryName;
	public Image bg;

	public void OnItemClick () {
		PlayerPrefs.SetString (CommonConfig.USER_CATEGORY_SELECTED, categoryName);
		CommonConfig.userData [CommonConfig.USER_CATEGORY_SELECTED] = categoryName;

		Debug.Log ("Category " + categoryName + " selected");

		if (!SystemInfo.supportsGyroscope) {
			Instantiate (Resources.Load ("Prefabs/AR/BrandLogoCanvas"));
		} else {
			ArReload.Invoke ();
		}

		this.transform.parent.transform.parent.gameObject.SetActive (false);
//		Destroy (this.transform.parent.transform.parent.gameObject);
	}

}
