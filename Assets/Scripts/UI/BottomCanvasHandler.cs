﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottomCanvasHandler : MonoBehaviour {

	public GameObject _uiCategoryCanvas;

	public void OnCategoryClick () {
		if (_uiCategoryCanvas.activeSelf) {
			_uiCategoryCanvas.SetActive (false);
		} else {
			_uiCategoryCanvas.SetActive (true);
		}
	}
}
