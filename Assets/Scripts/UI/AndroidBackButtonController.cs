﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class AndroidBackButtonController : Singleton<AndroidBackButtonController>
{
    public Stack<Action> stack = new Stack<Action>();

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && stack.Count > 0)
        {
            Pop();
        }
    }

    public void Push(Action action)
    {
        stack.Push(action);
    }

    public void Pop()
    {
        var action = stack.Pop();
        action();
    }
}
