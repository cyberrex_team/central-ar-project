﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Model;
using UnityEngine.UI;

public class BrandItemHandler : MonoBehaviour
{

    public string brandCode;
    public BrandDetail brandDetail;

    public Image qrcode;
    public Image logoImage;
    public Text title;
    public Text counter;

    void Start()
    {
        brandDetail = CommonConfig.brandData[brandCode];

        UpdateBrandDetail();
        UpdateLogo();
//		UpdateQR();
    }

    void UpdateBrandDetail()
    {
        title.text = brandDetail.name;
    }

    void UpdateLogo()
    {
        logoImage.sprite = Resources.Load<Sprite>("Sprites/" + brandDetail.logo);
    }

	void UpdateQR()
	{
		qrcode.sprite = Resources.Load<Sprite>("Sprites/QRcode/" + brandDetail.typeofBarCode);
	}

    public void OnBrandItemClick()
    {
        GameObject d = Instantiate(Resources.Load("Prefabs/UI/DialogCanvas")) as GameObject;
        DialogCanvasHandler dialog = d.GetComponent<DialogCanvasHandler>();
        dialog.brandCode = brandCode;
    }
}
