﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BrandImageItemHandler : MonoBehaviour
{

	public Image brandImage;
	public string brandName;

	// Use this for initialization
	void Awake ()
	{
		brandImage = GetComponent<Image> ();
	}

	void Start () {
		float rnd = Random.Range (1.3f, 2.2f);
		brandImage.transform.localScale *= rnd;
	}

	public void OnBrandClick ()
	{
		Debug.Log (brandName + ", Brand Clicked");
		DialogCanvasHandler dialog = Instantiate (Resources.Load ("Prefabs/UI/PreviewDialogCanvas") as GameObject).GetComponent<DialogCanvasHandler> ();
		dialog.brandCode = brandName;
		dialog._uiBrand = this.gameObject;
	}

	void BrandDestroy () {
		Destroy (this.gameObject);
	}
}
