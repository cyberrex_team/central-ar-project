﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Model;

public class InitBase : MonoBehaviour
{
	public GameObject consoleLog;

	public bool isReady = false;

	// Use this for initialization
	void Start ()
	{
		Init ();
	}

	void Init ()
	{

		ProductManager.Instance.Init ();

		//List Data
		if (!PlayerPrefs.HasKey (CommonConfig.PLAYER_INIT_APP)) {
			AppManager.Instance.Init ();
		} else {
		}
			
		#if UNITY_IOS || UNITY_ANDROID
//		Instantiate (consoleLog);
		#endif
	}



}
