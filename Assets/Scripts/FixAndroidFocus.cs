﻿using UnityEngine;
using System.Collections;
using Vuforia;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using UnityEngine.UI;

public class FixAndroidFocus : MonoBehaviour {

	void Start () {
	
	}

	void Update () {
		#if UNITY_ANDROID
		if(Input.GetMouseButton(0)){
			bool focusModeSet = CameraDevice.Instance.SetFocusMode( 
				CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
			if (!focusModeSet) {
				Debug.Log("Failed to set focus mode (unsupported mode).");
			}
		}
		#endif
	}

}
