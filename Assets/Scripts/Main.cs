﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using CurvedUI;

public class Main : MonoBehaviour, ITrackableEventHandler
{
	
	[SerializeField] Transform m_ARCamera;
	[SerializeField] ImageTargetBehaviour m_ImageTarget;
	[SerializeField] ImageTargetBehaviour m_ImageTargetCentral;

	public bool isMouseMoving = false;

	[SerializeField] Transform CameraObject;
	[SerializeField] float rotationMargin = 25;

	[SerializeField] Transform _uiCanvas;
	[SerializeField] Transform _uiCategory;
	[SerializeField] Transform _uiScanARCanvas;
	[SerializeField] Transform _uiMarkerDetect;
	[SerializeField] Transform _uiBottomCanvas;
	[SerializeField] Transform _uiTopCanvas;

	[SerializeField]
	public GameObject _spaceShip;
	public GameObject _worldSpace;
	public ARManager arManager;

	private float m_InitialYAngle = 0f;
	private float m_AppliedGyroYAngle = 0f;
	private float m_CalibrationYAngle = 0f;
	private Vector3 m_Offset;

	private void Start ()
	{
		Application.targetFrameRate	=	60;
		m_InitialYAngle = transform.eulerAngles.y;
		Input.gyro.enabled = true;

		Camera.main.fieldOfView = 15f;

		if (PlayerPrefs.GetInt (CommonConfig.USER_SCAN_DETECTED) == 0) {
			_uiScanARCanvas.gameObject.SetActive (true);
		}
	}

	private void OnEnable ()
	{
		m_ImageTarget.RegisterTrackableEventHandler (this);
		Debug.Log ("On Enable:: " + PlayerPrefs.GetInt (CommonConfig.USER_SCAN_DETECTED));

		if (PlayerPrefs.GetInt (CommonConfig.USER_SCAN_DETECTED) == 1) {
			EnableARExperience (true);
		}
	}

	void EnableARExperience (bool _status) {
		_uiScanARCanvas.transform.gameObject.SetActive (!_status);
		_uiBottomCanvas.transform.gameObject.SetActive (_status);
		_uiTopCanvas.transform.gameObject.SetActive (_status);

		if (!_uiCategory.transform.gameObject.activeSelf) {
			_uiCategory.transform.gameObject.SetActive (_status);
		}

		_spaceShip.SetActive (_status);
		_worldSpace.SetActive (_status);
	}

	private void OnDisable ()
	{
		m_ImageTarget.UnregisterTrackableEventHandler (this);
	}

	void ITrackableEventHandler.OnTrackableStateChanged (TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
	{
		bool _status = false;

		if (newStatus == TrackableBehaviour.Status.DETECTED ||
		    newStatus == TrackableBehaviour.Status.TRACKED ||
		    newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED) {

			_status = true;
			Debug.LogFormat (_status + " --- {0}, {1}", m_ImageTarget.TrackableName, newStatus.ToString ());

			EnableARExperience (_status);
//			_uiScanARCanvas.transform.gameObject.SetActive (!_status);
//			_uiBottomCanvas.transform.gameObject.SetActive (_status);
//			_uiTopCanvas.transform.gameObject.SetActive (_status);
//
//			if (!_uiCategory.transform.gameObject.activeSelf) {
//				_uiCategory.transform.gameObject.SetActive (_status);
//			}
//
//			_spaceShip.SetActive (_status);
//			_worldSpace.SetActive (_status);

			PlayerPrefs.SetInt (CommonConfig.USER_SCAN_DETECTED, 1);

			CalibrateYAngle ();
		} else {
//			_uiCanvas.transform.gameObject.SetActive (false);
//			_spaceShip.SetActive (false);
		}
	}

	void Update ()
	{
		ApplyGyroRotation ();
		ApplyCalibration ();

		if (isMouseMoving)
			m_ARCamera.localEulerAngles = new Vector3 (Input.mousePosition.y.Remap (0, Screen.height, rotationMargin, -rotationMargin),
				Input.mousePosition.x.Remap (0, Screen.width, -rotationMargin, rotationMargin),
				0);
	}

	public void CalibrateYAngle ()
	{
		m_CalibrationYAngle = m_AppliedGyroYAngle - m_InitialYAngle; // Offsets the y angle in case it wasn't 0 at edit time.
	}

	void ApplyGyroRotation ()
	{		
		transform.rotation = Input.gyro.attitude;
		transform.Rotate (0f, 0f, 180f, Space.Self); // Swap "handedness" of quaternion from gyro.
		transform.Rotate (90f, 180f, 0f, Space.World); // Rotate to make sense as a camera pointing out the back of your device.
		m_AppliedGyroYAngle = transform.eulerAngles.y; // Save the angle around y axis for use in calibration.
	}

	void ApplyCalibration ()
	{
		transform.Rotate (0f, -m_CalibrationYAngle, 0f, Space.World); // Rotates y angle back however much it deviated when calibrationYAngle was saved.
	}
}
