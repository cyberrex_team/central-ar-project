﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Model;

public class CommonConfig
{

	//Config
	public const double expiredTime = 86400;
	public const string EXPIRED_DATE = "_expireDate";

	//Data
	public static Dictionary<string, BrandDetail> brandData = new Dictionary<string, BrandDetail> ();
	public static List<string> userBrands = new List<string> ();

	//User Keys
	public const string PLAYER_INIT_APP = "PLAYER_INIT_APP";

	//Categories
	public const string BRAND_CATEGORY_MENS = "MENS";
	public const string BRAND_CATEGORY_JEANS = "JEANS";
	public const string BRAND_CATEGORY_SHOES = "SHOES";

	public static Dictionary<string, string> brandCategory = new Dictionary<string, string> ();

	//Users
	public const string USER_CATEGORY_SELECTED = "USER_CATEGORY_SELECTED";
	public const string USER_BRAND_SELECTED = "USER_BRAND_SELECTED";
	public static Dictionary<string, string> userData = new Dictionary<string, string> () {
		{ USER_CATEGORY_SELECTED, "" }
	};

	public const string USER_SCAN_DETECTED = "USER_SCAN_DETECTED";
}
