﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Model;

public class AppManager : Singleton<AppManager>
{

	// Use this for initialization
	public void Init ()
	{
		this.GetMyBrand ();
	}

	public List<BrandDetail> GetMyBrand() {
		List<BrandDetail> brands = new List<BrandDetail> ();

		foreach (BrandDetail brand in CommonConfig.brandData.Values) {
			if (PlayerPrefs.HasKey (brand.code) && PlayerPrefs.GetInt (brand.code) == 1 &&
			   !ProductManager.Instance.IsExpired (brand.code)) {
				brands.Add (brand);

//				Debug.LogFormat (" --- {0}:{1}, {2}", brand.code, brand.name, PlayerPrefs.GetString (brand.code + CommonConfig.EXPIRED_DATE));
				CommonConfig.userBrands.Add (brand.code);
			}
		}

		return brands;
	}

	public void AddBrand (string code)
	{
		if (!IsTaken (code)) {
			CommonConfig.userBrands.Add (code);

			double expire = TimeManager.Instance.GetUnixTimeStamp ();
			expire += CommonConfig.expiredTime;

			PlayerPrefs.SetInt (code, 1);
			PlayerPrefs.SetString (code + CommonConfig.EXPIRED_DATE, expire.ToString());
		}
	}

	public bool RemoveBrand (string code)
	{
		CommonConfig.userBrands.Remove (code);
		PlayerPrefs.DeleteKey (code);
		PlayerPrefs.DeleteKey (code + CommonConfig.EXPIRED_DATE);

		return true;
	}

	public bool IsTaken (string code)
	{
		foreach (string b in CommonConfig.userBrands) {
			if (b == code) {
				return true;
			}
		}

		return false;
	}


}
