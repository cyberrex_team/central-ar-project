﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitManager : MonoBehaviour {
	
	[SerializeField]
	public GameObject[] loadObject;

	// Use this for initialization
	void Start () {
		foreach (GameObject obj in loadObject) {
			Instantiate (obj);
		}
	}
}
