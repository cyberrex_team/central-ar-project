﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Model;

public class ARManager : MonoBehaviour
{

	[SerializeField] Transform _uiBrandCanvas;
	[SerializeField] Transform _uiMarkerDetectCanvas;
	[SerializeField] Transform _uiCategoryCanvas;

	[SerializeField]
	public Transform[] _slots;

	public List<int> _emptySlots = new List<int> ();

	private List<BrandDetail> _brands = new List<BrandDetail> ();

	// Use this for initialization
	void Start ()
	{
		CategoryItemHandler.ArReload += OnReloaded;
	}

	void OnDestroy () {
		CategoryItemHandler.ArReload -= OnReloaded;
	}

	void OnReloaded ()
	{
		Debug.Log (" -- Reloaded");
		_uiCategoryCanvas.transform.gameObject.SetActive (false);
		ClearBrandImages ();
		Reload ();
	}

	void ClearBrandImages () {		
		foreach (int t in _emptySlots) {
			if (_slots [t].GetComponentInChildren<BrandImageItemHandler> () != null) {
				Destroy (_slots [t].GetComponentInChildren<BrandImageItemHandler> ().gameObject);
			}
		}

		_emptySlots.Clear ();
		_brands.Clear ();
	}

	public void Reload ()
	{
		_uiBrandCanvas.transform.gameObject.SetActive (true);
		_uiMarkerDetectCanvas.transform.gameObject.SetActive (true);

		StartCoroutine ("voidDetectCanvas");

//		Debug.Log ("Category selected :: " + PlayerPrefs.GetString (CommonConfig.USER_CATEGORY_SELECTED));
		_brands = ProductManager.Instance.GetProductByCategory (PlayerPrefs.GetString (CommonConfig.USER_CATEGORY_SELECTED), true);

		foreach (BrandDetail brand in _brands) {
			Transform slot = FindSlots (brand);
		}
	}

	IEnumerator voidDetectCanvas () {
		yield return new WaitForSeconds (3f);

		_uiMarkerDetectCanvas.transform.gameObject.SetActive (false);
	}

	Transform FindSlots (BrandDetail brand)
	{
		
		int rnd = Random.Range (0, _slots.Length);
		if (_emptySlots.Contains (rnd)) {
			return FindSlots (brand);
		}

		GameObject _b = Instantiate (Resources.Load ("Prefabs/UI/BrandImageItemList")) as GameObject;
		_b.name = brand.name;
		_b.transform.SetParent (_slots[rnd], false);

		BrandImageItemHandler _item = _b.GetComponent<BrandImageItemHandler> ();
		_item.brandImage.sprite = Resources.Load<Sprite> ("Sprites/" + brand.logo);
		_item.brandImage.gameObject.SetActive (false);

		new WaitForSeconds (0.1f);
		_item.brandImage.gameObject.SetActive (true);
		_item.brandName = brand.code;

		_emptySlots.Add (rnd);

		return _slots [rnd];
	}

}
