﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Globalization;

public class TimeManager : Singleton<TimeManager>
{

	public string url = "http://api.timezonedb.com/?zone=Europe/London&format=json&key=0TEFKLWMVX00";
	public double currentTime;
	bool isInit = false;
	float counter = 1;

	public bool isReady;

	public void Init ()
	{
		isInit = true;

		// TEST FOR DAILY BONUS
		currentTime = GetUnixTimeStamp ();
	}

	public double GetUnixTimeStamp ()
	{
		DateTime epochStart = new DateTime (1970, 1, 1);
		double timestamp = (DateTime.Now - epochStart).TotalSeconds;
		return System.Math.Floor (timestamp);
	}

	public double GetUnixTimeStamp (DateTime datetime)
	{
		DateTime epochStart = new DateTime (1970, 1, 1);
		double timestamp = (datetime - epochStart).TotalSeconds;
		return System.Math.Floor (timestamp);
	}

	public DateTime GetDateTime (double timestamp = 0)
	{
		if (timestamp == 0)
			timestamp = GetCurrentTime ();

		DateTime dt = new DateTime (1970, 1, 1, 0, 0, 1, DateTimeKind.Utc).AddSeconds (timestamp);
		return dt;
	}

	public int getWeekOfYear (double timestamp = 0)
	{
		if (timestamp == 0)
			timestamp = GetCurrentTime ();

		DateTimeFormatInfo df = DateTimeFormatInfo.CurrentInfo;
		Calendar cl = df.Calendar;
		DateTime dt = GetDateTime (timestamp);

		return cl.GetWeekOfYear (dt, df.CalendarWeekRule, df.FirstDayOfWeek);
	}

	public bool IsTimeExpired (double next, double current = 0, bool isSameday = false)
	{
		bool expired = true;

		if (current == 0)
			current = GetUnixTimeStamp ();

		TimeSpan timeLeft = TimeSpan.FromSeconds (next - current);

		if (isSameday) {
			DateTime today = GetDateTime (current);
			DateTime nextDay = GetDateTime (next);

//			Debug.Log (today + ", " + nextDay);
			if (today.DayOfYear == nextDay.DayOfYear) {
				return false;	
			}

			return true;
		}

		if (timeLeft.Days > 0 || timeLeft.Hours > 0 || timeLeft.Minutes > 0) {
			expired = false;
		}

//		Debug.Log (current + ", " + next);
//		Debug.Log (timeLeft);
//		Debug.LogFormat ("Days {0}, Hours {1}, Min {2}", timeLeft.Days, timeLeft.Hours, timeLeft.Minutes);
//		Debug.LogFormat (expired + ", Total(s) mins {0}", timeLeft.TotalMinutes);

		return expired;
	}

	void Update ()
	{
		if (counter >= 1) { 
			currentTime += 1;
			counter = 0;
			return;
		}

		counter += Time.deltaTime;
	}

	public double GetTimeLeft (double current = 0, double next = 0)
	{
		DateTime nextDate = GetDateTime (current).AddSeconds ((double)next);
		double timestamp = GetUnixTimeStamp (nextDate);

		return timestamp;
	}

	public TimeSpan getOneDayLeft ()
	{
		DateTime d1 = GetDateTime ();
		DateTime d2 = new DateTime (d1.Year, d1.Month, d1.Day, 23, 59, 59);

		double diff = (d2 - d1).TotalSeconds;
		TimeSpan timeleft = TimeSpan.FromSeconds (diff);

		return timeleft;
	}

	//return Now
	public double GetCurrentTime ()
	{
		return currentTime;
	}
}
