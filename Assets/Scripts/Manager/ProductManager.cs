﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Model;

public class ProductManager : Singleton<ProductManager> {

	public void Init () {
		// App Data
		LoadBrandCategory ();
		LoadBrandList ();
	}

	void LoadBrandCategory () {		
		CommonConfig.brandCategory [CommonConfig.BRAND_CATEGORY_MENS] = "MEN'S CASUAL";
		CommonConfig.brandCategory [CommonConfig.BRAND_CATEGORY_JEANS] = "JEANS";
		CommonConfig.brandCategory [CommonConfig.BRAND_CATEGORY_SHOES] = "UNISEX SHOES";
	}

	void LoadBrandList ()
	{
		BrandData brandList = XmlUtil.DeserializeModel<BrandData> ("Setting/Brand") as BrandData;
		foreach (BrandDetail brand in brandList.brands) {
//			Debug.Log (" --- " + brand.code + ", " + brand.name + " :: " + brand.logo);
			CommonConfig.brandData [brand.code] = brand;
		}

		Debug.Log ("brands count :: " + brandList.brands.Count);
	}

	public bool IsExpired (string code) {
		bool isExpired = true;

		double timer = TimeManager.Instance.GetUnixTimeStamp ();

		if (!PlayerPrefs.HasKey (code + CommonConfig.EXPIRED_DATE)) {
			PlayerPrefs.SetString (code + CommonConfig.EXPIRED_DATE, timer.ToString ());
		} else {
			timer = double.Parse (PlayerPrefs.GetString (code + CommonConfig.EXPIRED_DATE));
		}

		double time_added = timer + CommonConfig.expiredTime;

//		Debug.LogFormat ("timestamp {0}", timer);
//		Debug.LogFormat ("timeadded {0}", time_added);

		if (PlayerPrefs.HasKey (code + CommonConfig.EXPIRED_DATE)) {
//			Debug.LogFormat ("Expire Time :: {0} ", PlayerPrefs.GetString (code + CommonConfig.EXPIRED_DATE));
			if (TimeManager.Instance.IsTimeExpired (time_added, 0)) {
				Debug.Log (code + " -- Expired!! ");
				PlayerPrefs.DeleteKey (code);
				PlayerPrefs.DeleteKey (code + CommonConfig.EXPIRED_DATE);
			} else {
				isExpired = false;
			}
		}

		return isExpired;
	}

	public List<BrandDetail> GetProductByCategory (string category, bool ingoreDuplicated = false) {
		List<BrandDetail> brands = new List<BrandDetail> ();
		bool hasItem = false;

		Debug.Log ("Brand Cound :: " + brands.Count);
		foreach (BrandDetail brand in CommonConfig.brandData.Values) {
			hasItem = false;

			if (PlayerPrefs.HasKey(brand.code) && (PlayerPrefs.GetInt(brand.code) == 1)) {
				hasItem = true;
			}

			if (brand.category == category && !hasItem || (!ingoreDuplicated)) {
//				Debug.Log (" --- " + brand.code + ", " + brand.name + " :: " + brand.logo);
				brands.Add (brand);
			}
		}

		return brands;
	}
}
