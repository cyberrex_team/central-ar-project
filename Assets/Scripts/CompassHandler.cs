﻿using UnityEngine;
using System.Collections;
using Location;

public class CompassHandler : MonoBehaviour
{
    public Transform CompassSetTransform;
    public Transform ArrowTransform;
    public Transform ShadowTransform;

    private float targetLatitude;
    private float targetLongitude;
    private Quaternion finalRotation;
    private Quaternion finalArrowRotation;    
    public float compassArHeading = 55.5f;

    // Use this for initialization
    public void Init()
    {        
        Debug.Log("-> compass handler subdcribed!");
        Input.compass.enabled = true;
        Input.gyro.enabled = true;

    }

    void StartFollowingAR()
    {
        Debug.Log("compass AR started following");
        
        StartCoroutine(FollowAR());
    }

    void StartFollowingGPS()
    {
        Debug.Log("Compass GPS started following");        
        StartCoroutine(FollowGPS());
    }

    public void StopFollowing()
    {
		Debug.Log("Compass stopped following");        
        StopAllCoroutines();
    }

    public void SetArHeading(float headingValue)
    {
        compassArHeading = headingValue;
    }

    public void SetTargetLocation(float receivedLongtitiude, float receivedLatitude)
    {
        targetLongitude = receivedLongtitiude;
        targetLatitude = receivedLatitude;
    }

    IEnumerator FollowAR()
    {

        while (true)
        {
            var angle = Input.compass.trueHeading - compassArHeading;

            if (angle < 0)
            {
                angle = angle + 360;
            }

            angle = 360 - angle;
            finalArrowRotation = Quaternion.Euler(new Vector3(0f, 0f, -angle));

            var gravity = Input.gyro.gravity;
            finalRotation = Quaternion.AngleAxis(0, Vector3.forward) *
                Quaternion.AngleAxis((gravity.z + 1) * 90, Vector3.right) *
                Quaternion.AngleAxis(gravity.x * 90, Vector3.up);

            yield return new WaitForSeconds(0.1f);
        }
    }

    IEnumerator FollowGPS()
    {
        var angleCalculator = new CalculateAngle(targetLatitude, targetLongitude);
        while (true)
        {
            var angle = angleCalculator.AngleWithCompass(
                Input.location.lastData.latitude,
                Input.location.lastData.longitude,
                Input.compass.trueHeading);
            finalArrowRotation = Quaternion.Euler(new Vector3(0f, 0f, -angle));

            var gravity = Input.gyro.gravity;
            finalRotation = Quaternion.AngleAxis(0, Vector3.forward) *
                Quaternion.AngleAxis((gravity.z + 1) * 90, Vector3.right) *
                Quaternion.AngleAxis(gravity.x * 90, Vector3.up);

            yield return new WaitForSeconds(0.1f);
        }
    }

    // Update is called once per frame
    void Update()
    {
        CompassSetTransform.localRotation = Quaternion.Slerp(CompassSetTransform.localRotation, finalRotation, 0.2f);
        ArrowTransform.localRotation = Quaternion.Slerp(ArrowTransform.localRotation, finalArrowRotation, 0.2f);
        ShadowTransform.localRotation = Quaternion.Slerp(ArrowTransform.localRotation, finalArrowRotation, 0.2f);
    }
}
