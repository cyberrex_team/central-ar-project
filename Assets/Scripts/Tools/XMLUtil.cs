﻿using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using System;
using Model;

public static class XmlUtil
{

	public static T DeserializeModel<T> (string filepath)
	{

		T t;

		XmlDocument xmlDoc = new XmlDocument ();

		TextAsset textXML = Resources.Load (filepath, typeof(TextAsset)) as TextAsset;
		if (textXML) {

			XmlSerializer x = new XmlSerializer (typeof(T));

			using (var reader = new StringReader(textXML.text)) {
				t = (T)x.Deserialize (reader);
			}

			return t;
		} else {
			return default(T);
		}

	}

	public static void WriteToXml ()
	{

		string filepath = Application.dataPath + @"/Resources/Data/State/101.xml";
		FileInfo t = new FileInfo(filepath); 

		XmlDocument xmlDoc = new XmlDocument();
		Debug.Log ("Path:: " + filepath);
		StreamWriter writer;

		if (!t.Exists) {
			t.Delete ();
			writer = t.CreateText ();
			writer.Write("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
			writer.WriteLine ("<State></State>");
			writer.Close();
		}

		if (File.Exists (filepath)) {
			xmlDoc.Load (filepath);

			XmlElement elmRoot = xmlDoc.DocumentElement;

			elmRoot.RemoveAll (); // remove all inside the transforms node.

			XmlElement elmNew = xmlDoc.CreateElement ("rotation"); // create the rotation node.

			XmlElement rotation_X = xmlDoc.CreateElement ("x"); // create the x node.
			rotation_X.InnerText = "1"; // apply to the node text the values of the variable.

			XmlElement rotation_Y = xmlDoc.CreateElement ("y"); // create the y node.
			rotation_Y.InnerText = "1"; // apply to the node text the values of the variable.

			XmlElement rotation_Z = xmlDoc.CreateElement ("z"); // create the z node.
			rotation_Z.InnerText = "1"; // apply to the node text the values of the variable.

			elmNew.AppendChild (rotation_X); // make the rotation node the parent.
			elmNew.AppendChild (rotation_Y); // make the rotation node the parent.
			elmNew.AppendChild (rotation_Z); // make the rotation node the parent.
			elmRoot.AppendChild (elmNew); // make the transform node the parent.

			xmlDoc.Save (filepath); // save file.

		}



		 
		Debug.Log("File written."); 
	}

}

