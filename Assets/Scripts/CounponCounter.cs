﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CounponCounter : MonoBehaviour {

	Text number;

	// Use this for initialization
	void Start () {
		number = GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		number.text = AppManager.Instance.GetMyBrand ().Count.ToString ();
	}
}
