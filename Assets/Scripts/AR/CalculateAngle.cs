﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Location
{
	public class CalculateAngle	
	{
		private float latitude;
		private float longitude;

		public CalculateAngle(float latitude, float longitude)
		{
			this.latitude = latitude;
			this.longitude = longitude;
		}

		public float Bearing(float otherLatitude, float otherLongitude)
		{
			float endLat = this.latitude * Mathf.Deg2Rad;
			float endLong = this.longitude * Mathf.Deg2Rad;
			float startLat = otherLatitude * Mathf.Deg2Rad;
			float startLong = otherLongitude * Mathf.Deg2Rad;

			float bearing = 0f;
			float X = 0f;
			float Y = 0f;

			X = Mathf.Cos(endLat) * Mathf.Sin(endLong - startLong);
			Y = Mathf.Cos(startLat) * Mathf.Sin(endLat) - Mathf.Sin(startLat) * Mathf.Cos(endLat) * Mathf.Cos(endLong - startLong);
			bearing = Mathf.Atan2(X, Y) * Mathf.Rad2Deg;

			return bearing;
		}

		public float AngleWithCompass(float otherLatitude, float otherLongitude, float trueNorth)
		{

			var res = Bearing(otherLatitude, otherLongitude);
			float NBearing = -trueNorth + res;

			//		wrap in 0-360
			if (NBearing < 360f)
			{
				NBearing = NBearing + 360f;
			}
			float WrappedNBearing = (NBearing < 0f) ? NBearing + 360f : NBearing;

			return WrappedNBearing;
		}
	}
}
