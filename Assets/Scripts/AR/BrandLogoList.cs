﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Model;

public class BrandLogoList : MonoBehaviour {

	public Transform content;
	public string categoryName;
	private List<BrandDetail> _brands = new List<BrandDetail> ();

	// Use this for initialization
	void Start () {
		categoryName = "BAGS";
		ClearBrandImages ();
	}

	public void Hide () {
		Destroy (this.gameObject);
	}

	void ClearBrandImages () {
		BrandLogoItem[] lists = content.GetComponentsInChildren<BrandLogoItem> ();

		foreach (BrandLogoItem t in lists) {
			Destroy (t.transform.gameObject);
		}

		_brands.Clear ();

		LoadBrandsList ();
	}

	void LoadBrandsList () {
		_brands = ProductManager.Instance.GetProductByCategory (PlayerPrefs.GetString (CommonConfig.USER_CATEGORY_SELECTED), true);

		foreach (BrandDetail brand in _brands) {
			GameObject _b = Instantiate (Resources.Load ("Prefabs/AR/BrandLogoListItem")) as GameObject;
			BrandLogoItem bItem = _b.GetComponent<BrandLogoItem> ();
			bItem.brandCode = brand.code;
			_b.name = brand.name;

			_b.transform.localScale = Vector3.one;
			_b.transform.SetParent (content, false);


		}
	}

}
