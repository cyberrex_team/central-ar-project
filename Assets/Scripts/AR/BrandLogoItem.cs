﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Model;

public class BrandLogoItem : MonoBehaviour {

	public Image brandImage;
	public string brandCode;
	BrandDetail brandDetail;

	// Use this for initialization
	void Awake ()
	{		
		
	}

	void Start () {
		brandDetail = CommonConfig.brandData [brandCode];
		UpdateLogo ();
	}

	void UpdateLogo () {
		brandImage.sprite = Resources.Load<Sprite> ("Sprites/" + brandDetail.logo);
	}

	public void OnBrandClick ()
	{
		Debug.Log (brandCode + ", Brand Clicked");
		DialogCanvasHandler dialog = Instantiate (Resources.Load ("Prefabs/UI/PreviewDialogCanvas") as GameObject).GetComponent<DialogCanvasHandler> ();
		dialog.brandCode = brandCode;
		dialog._uiBrand = this.gameObject;
	}

	void BrandDestroy () {
		Destroy (this.gameObject);
	}
}
