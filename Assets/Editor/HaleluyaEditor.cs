﻿using UnityEngine;
using System;
using System.Collections;
using UnityEditor;

public class HaleluyaEditor : EditorWindow
{
    private const string PARAM_SAVE = "Save";

// 	//-------------------------------------------------------------------------------------

	[MenuItem("Haleluya/Wipe Data                           &3")]
	public static void DeleteAllPlayerPrefs()
	{
		PlayerPrefs.DeleteAll();
		Debug.LogError("DELETE ALL PLAYER PREFS DATA");
	}

    //-------------------------------------------------------------------------------------

    [MenuItem("Haleluya/ActiveToggle :                                   &a")]
    public static void ToggleActivationSelection()
    {
        var go = Selection.activeGameObject;
        go.SetActive(!go.activeSelf);
    }

    [MenuItem("Haleluya/ActiveToggleMultiple:                     &s")]
    public static void ToggleActivationMultiple()
    {
        foreach (GameObject go in Selection.gameObjects)
            go.SetActive(!go.activeSelf);
    }

}
